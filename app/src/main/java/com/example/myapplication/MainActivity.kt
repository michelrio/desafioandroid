package com.example.myapplication

import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.model.*
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*
import okhttp3.OkHttpClient
import okhttp3.Request
import java.io.IOException
import com.example.myapplication.Database.DatabaseHelper
import com.google.android.gms.maps.*
import java.util.*
import kotlin.collections.ArrayList
import android.text.SpannableStringBuilder


class MainActivity : AppCompatActivity() {

    private val LOCATION_REQUEST_CODE = 101
    lateinit var  mapFragment: SupportMapFragment
    lateinit var googleMap: GoogleMap
    lateinit var currentTime: Date
    var lat : Double = 0.0
    var long : Double = 0.0
    lateinit var marker: Marker
    var marked = 0
    var latMark : Double = 0.0
    var longMark : Double = 0.0
    var myLocal = LatLng(0.0,0.0)
    var myDestination = LatLng(0.0,0.0)
    var polyline : Polyline? = null
    var activeRoute = false
    var distance : String = ""
    var travelTime : String = ""
    var clickMap : Boolean = false

    lateinit var databaseHelper: DatabaseHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mapFragment = supportFragmentManager.findFragmentById(R.id.mapView) as SupportMapFragment
        mapFragment.getMapAsync(OnMapReadyCallback {
            googleMap = it
            googleMap.isMyLocationEnabled = true

            currentTime = Calendar.getInstance().time

            databaseHelper = DatabaseHelper(this)


            //Permissão Location
            val permission = ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION)
            if (permission == PackageManager.PERMISSION_GRANTED) {
                //
            } else {
                requestPermission(
                    android.Manifest.permission.ACCESS_FINE_LOCATION,
                    LOCATION_REQUEST_CODE
                )
            }

            //Localização atual
            val loc = LocationServices.getFusedLocationProviderClient(this)
            loc.lastLocation
                .addOnSuccessListener { location: Location? ->
                    if (location != null) {
                        lat = location.latitude
                        long = location.longitude
                    }else{
                        lat =  00.00
                        long = 00.00
                    }
                    myLocal = LatLng(lat, long)

                    googleMap.addMarker(
                                    MarkerOptions()
                                    .position(myLocal)
                                    .title("Minha Localização"))
                    //.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
                    googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(myLocal, 10f))
                    getLocal(lat, long)


                    googleMap.setOnMapClickListener { point ->

                        googleMap.getUiSettings().setAllGesturesEnabled(false)

                        clickMap = true

                        if (activeRoute == true){
                            polyline?.remove()
                            activeRoute = false
                        }

                        if (marked == 1) {
                            marker.remove()
                            marked = 0
                        }
//                        removeMarkers()
                        if (marked == 0) {
                            var clickMark = MarkerOptions()
                                .position(LatLng(point.latitude, point.longitude))
                                .title("Meu destino!")

                            latMark = point.latitude
                            longMark = point.longitude
                            marked = 1
                            //googleMap.addMarker(marker)
                            marker = googleMap.addMarker(clickMark)
                            marker
                            myDestination = LatLng(latMark,longMark)

                            getLocal(latMark, longMark)
                        }

                    }

                }
        })

        bt_getlatlong.setOnClickListener(){
            var end = tv_destination.text.toString()
            getLatLong(end)
            val URL = getDirectionURL(myLocal, getLatLong(end)!!)
            GetDirection(URL).execute()
        }

        bt_history.setOnClickListener(){
            var intent =  Intent(this, HistoryActivity::class.java)
            startActivity(intent)
        }

        bt_save.setOnClickListener(){
            insertFunction()
        }

    }



    private fun requestPermission(permissionType: String, requestCode: Int) {
        ActivityCompat.requestPermissions(this, arrayOf(permissionType), requestCode)
    }

    fun getLocal( latMark: Double, longMark: Double) {
        var addresses: List<Address>? = null
        var lat1: Double = latMark
        var long1: Double =  longMark


        try {
            addresses = Geocoder(this).getFromLocation(lat1, long1, 1)

            val country: String? = addresses[0].countryName
            val postalCode: String? = addresses[0].postalCode
            val knowName: String? = addresses[0].featureName
            var state: String? = addresses[0].adminArea
            var city: String? = addresses[0].subAdminArea
            val cityN: String? = addresses[0].locality
            var neighborhood: String?  = addresses[0].subLocality

            if (addresses == null) {
                Toast.makeText(applicationContext, "Marcação não permitida.", Toast.LENGTH_LONG).show()
            } else {
                var address = addresses[0].getAddressLine(0)
                if (address == null) {
                    address = "ignorado"
                } else {
                    address = addresses!![0].getAddressLine(0)
                }

                if (addresses[0].subLocality == null) {
                    neighborhood = "ignorado"
                } else {
                    neighborhood = addresses[0].subLocality
                }

                if (addresses[0].locality == null) {
                    city = addresses[0].subAdminArea
                } else {
                    city = addresses[0].locality
                }
                if (addresses[0].subAdminArea == null) {
                    city = "ignorado"
                }

                if (addresses[0].adminArea == null) {
                    state = "ignorado"
                } else {
                    state = addresses[0].adminArea
                }

                if(clickMap == true){
                    val editable = SpannableStringBuilder(" "+address)
                    tv_destination.text = editable
                    clickMap = false

                    val URL = getDirectionURL(myLocal, myDestination)
                    GetDirection(URL).execute()


                } else{
                    tv_myLocal.text = " "+address
                }
            }

        } catch (e: IOException) {
            e.printStackTrace()
            Toast.makeText(applicationContext, "Localização não carregada!", Toast.LENGTH_LONG).show()
        }

    }


    fun getDirectionURL (origin:LatLng, dest: LatLng?) : String {
        return "https://maps.googleapis.com/maps/api/directions/json?origin=" +
                "${origin.latitude}," +
                "${origin.longitude}" +
                "&destination=" +
                "${dest?.latitude}," +
                "${dest?.longitude}" +
                "&sensor=false&mode=driving" +
                "&key="+getString(R.string.google_maps_key)
    }

     inner class GetDirection(val URL : String) : AsyncTask<Void, Void, List<List<LatLng>>>(){
        override fun doInBackground(vararg params: Void?): List<List<LatLng>> {
            val client = OkHttpClient()
            val request = Request.Builder().url(URL).build()
            val response = client.newCall(request).execute()
            var data = response.body()!!.string()
            val result = ArrayList<List<LatLng>>()
            try {
                val respObj = Gson().fromJson(data,GoogleMapDTO::class.java)
                val path = ArrayList<LatLng>()
                for(i in 0 ..(respObj.routes[0].legs[0].steps.size-1)){

                    //Rota
                    path.addAll(decodePolyline(respObj.routes[0].legs[0].steps[i].polyline.points))

                }

//                tvStart.text = myLocal.toString()
//                tvDestination.text = myDestination.toString()

                //distância
                distance = respObj.routes[0].legs[0].distance.text
                tvDistance.text = distance
                //tempo
                travelTime = respObj.routes[0].legs[0].duration.text
                tvTime.text = travelTime

//              tvDate.text = currentTime.toString()

                result.add(path)
            }catch (e:Exception){
                e.printStackTrace()
            }
            return result
        }

        override fun onPostExecute(result: List<List<LatLng>>) {
            val lineoption = PolylineOptions()

            for (i in result.indices){
                lineoption.addAll(result[i])
                lineoption.width(10f)
                lineoption.color(Color.MAGENTA)
                lineoption.geodesic(true)
            }
            polyline = googleMap.addPolyline(lineoption)
            googleMap.setOnPolylineClickListener {polyline}
            activeRoute = true
            googleMap.getUiSettings().setAllGesturesEnabled(true)
        }
    }

      fun decodePolyline(encoded: String): List<LatLng> {

        val poly = ArrayList<LatLng>()
        var index = 0
        val len = encoded.length
        var lat = 0
        var lng = 0

        while (index < len) {
            var b: Int
            var shift = 0
            var result = 0
            do {
                b = encoded[index++].toInt() - 63
                result = result or (b and 0x1f shl shift)
                shift += 5
            } while (b >= 0x20)
            val dlat = if (result and 1 != 0) (result shr 1).inv() else result shr 1
            lat += dlat

            shift = 0
            result = 0
            do {
                b = encoded[index++].toInt() - 63
                result = result or (b and 0x1f shl shift)
                shift += 5
            } while (b >= 0x20)
            val dlng = if (result and 1 != 0) (result shr 1).inv() else result shr 1
            lng += dlng

            val latLng = LatLng((lat.toDouble() / 1E5),(lng.toDouble() / 1E5))
            poly.add(latLng)
        }

        return poly
    }


    fun getLatLong(strAddress: String): LatLng? {
        var coder = Geocoder(this)
        var address: List<Address>
        var p1: LatLng = LatLng(0.0, 0.0)

        try {
            address = coder.getFromLocationName(strAddress, 5)

            if (address == null) {
                return null
            }

            var location = address.get(0)
            location.latitude
            location.longitude

            p1 = LatLng(location.getLatitude(), location.getLongitude())

        } catch (e: IOException) {
            e.printStackTrace()
        }

        if (activeRoute == true) {
            polyline?.remove()
            activeRoute = false
        }
        if (marked == 1) {
            marker.remove()
            marked = 0
        }
        activeRoute = true
        marked = 1

        var clickMark = MarkerOptions()
            .position(p1)
            .title("Meu destino!")
        marker = googleMap.addMarker(clickMark)
        marker

        return p1
    }


    private fun insertFunction(){
        val strStart = myLocal.toString()
        val strDestination = myDestination.toString()
        val strDate = currentTime.toString()
        val strDistance = distance
        val strTime =  travelTime

        val result: Boolean = databaseHelper.insertData(strStart, strDestination, strDate, strDistance, strTime)

        when{
            result -> Toast.makeText(applicationContext,"Salvo com sucesso!", Toast.LENGTH_SHORT).show()
            else -> Toast.makeText(applicationContext,"Falha na gravação!", Toast.LENGTH_SHORT).show()
        }
    }

}
