package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import com.example.myapplication.Database.DatabaseHelper

class HistoryActivity : AppCompatActivity() {

    lateinit var databaseHelper : DatabaseHelper
    lateinit var result : TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_history)

        databaseHelper = DatabaseHelper(this)
        result = findViewById(R.id.tv_resultText)

        readDataFunction()
    }

    private fun readDataFunction() {
        val data = databaseHelper.readData()
        val stringBuffer = StringBuffer()

        if( data != null && data.count > 0)
        {
            while (data.moveToNext())
            {
                stringBuffer.append("ID: ${data.getString(0)}\n")
                stringBuffer.append("ORIGEM: ${data.getString(1)}\n")
                stringBuffer.append("DESTINO: ${data.getString(2)}\n")
                stringBuffer.append("DATA: ${data.getString(3)}\n")
                stringBuffer.append("DISTÂNCIA: ${data.getString(4)}\n")
                stringBuffer.append("TEMPO: ${data.getString(5)}\n\n")
            }
            result.text = stringBuffer.toString()
        }
    }
}
