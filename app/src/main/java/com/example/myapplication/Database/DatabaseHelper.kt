package com.example.myapplication.Database

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.provider.BaseColumns
import com.example.myapplication.Database.DatabaseContainer.PersonTable.Companion.DATE_COLUMN
import com.example.myapplication.Database.DatabaseContainer.PersonTable.Companion.DESTINATION_COLUMN
import com.example.myapplication.Database.DatabaseContainer.PersonTable.Companion.DISTANCE_COLUMN
import com.example.myapplication.Database.DatabaseContainer.PersonTable.Companion.START_COLUMN
import com.example.myapplication.Database.DatabaseContainer.PersonTable.Companion.TABLE_NAME
import com.example.myapplication.Database.DatabaseContainer.PersonTable.Companion.TIME_COLUMN

class DatabaseHelper(context: Context): SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {

    override fun onCreate(db: SQLiteDatabase?){
        val personTable = "CREATE TABLE " +
                TABLE_NAME +"("+
                BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"+
                START_COLUMN + " TEXT,"+
                DESTINATION_COLUMN + " TEXT," +
                DATE_COLUMN + " TEXT," +
                DISTANCE_COLUMN + " TEXT," +
                TIME_COLUMN + " TEXT" + ")"

        db!!.execSQL(personTable)
    }
    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int){
        db!!.execSQL("DROP TABLE IF EXISTS $TABLE_NAME")
    }

    //SQL Create Data Function
    fun insertData (start: String, destination: String, date: String, distance: String, time:String):Boolean
    {
        val db: SQLiteDatabase = this.writableDatabase
        val contentValues = ContentValues()
        contentValues.put(START_COLUMN, start)
        contentValues.put(DESTINATION_COLUMN, destination)
        contentValues.put(DATE_COLUMN, date)
        contentValues.put(DISTANCE_COLUMN, distance)
        contentValues.put(TIME_COLUMN, time)
        val insert_data = db.insert(TABLE_NAME, null, contentValues)
        db.close()

        return !insert_data.equals(-1)
    }

    //SQL Read Data Function
    fun readData(): Cursor {
        val db: SQLiteDatabase = this.writableDatabase
        val read: Cursor = db.rawQuery("SELECT * FROM $TABLE_NAME", null)
        return read
    }

    companion object {
        private const val DATABASE_NAME = "DesafioAndroid.db"
        private const val DATABASE_VERSION = 1

    }

}