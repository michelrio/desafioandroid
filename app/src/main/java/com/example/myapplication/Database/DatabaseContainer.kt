package com.example.myapplication.Database

import android.provider.BaseColumns

object DatabaseContainer{

    class PersonTable: BaseColumns{
        companion object{
            val TABLE_NAME = "Person_table"
            val START_COLUMN = "ORIGEM"
            val DESTINATION_COLUMN = "DESTINO"
            val DATE_COLUMN = "DATA"
            val DISTANCE_COLUMN = "DISTANCIA"
            val TIME_COLUMN = "TEMPO"
        }
    }
}